// ignore_for_file: constant_identifier_names, camel_case_types, no_leading_underscores_for_local_identifiers

import 'package:firebase_auth/firebase_auth.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rental_car/providers/firebase_provider/firebase_provider.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'authentication_bloc.g.dart';
part 'authentication_bloc.freezed.dart';

enum AUTHENTICATION_STATUS { NOT_REGISTER, REGISTERED, CHECKING }

@freezed
class AuthenticationState with _$AuthenticationState {
  const factory AuthenticationState({
    required AUTHENTICATION_STATUS status,
    required User? user,
    required FirebaseAuth $firebaseAuth,
  }) = _AuthenticationState;
}

@riverpod
class AuthenticationBloc extends _$AuthenticationBloc {
  @override
  AuthenticationState build() {
    final firebaseAuth = ref.watch(firebaseAuthProvider);
    return AuthenticationState(
      status: AUTHENTICATION_STATUS.CHECKING,
      user: null,
      $firebaseAuth: firebaseAuth,
    );
  }

  Future checkAuth() async {
    final user = state.$firebaseAuth.currentUser;
    await Future.delayed(const Duration(seconds: 1));
    if (user == null) {
      state = state.copyWith(
        status: AUTHENTICATION_STATUS.NOT_REGISTER,
      );
    } else {
      state = state.copyWith(
        status: AUTHENTICATION_STATUS.REGISTERED,
        user: user,
      );
    }
  }

  Future logout() async {
    await state.$firebaseAuth.signOut();
    await Future.delayed(const Duration(seconds: 2));
    state = state.copyWith(
      status: AUTHENTICATION_STATUS.CHECKING,
      user: null,
    );
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_bloc.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authenticationBlocHash() =>
    r'46b26a03b228f0c9aad209f56179ffa8453f8dd5';

/// See also [AuthenticationBloc].
@ProviderFor(AuthenticationBloc)
final authenticationBlocProvider = AutoDisposeNotifierProvider<
    AuthenticationBloc, AuthenticationState>.internal(
  AuthenticationBloc.new,
  name: r'authenticationBlocProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$authenticationBlocHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AuthenticationBloc = AutoDisposeNotifier<AuthenticationState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member

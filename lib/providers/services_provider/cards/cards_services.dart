// ignore_for_file: unused_field, unnecessary_cast
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rental_car/constants/firebase.constants.dart';
import 'package:rental_car/models/card/card_model.dart';
import 'package:rental_car/providers/firebase_provider/firebase_provider.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

final cardsServiceProvider = Provider((ref) {
  return CardsService(
    firestore: ref.watch(firebaseFirestoreProvider),
    firebaseAuth: ref.watch(firebaseAuthProvider),
  );
});

class CardsService {
  final FirebaseFirestore firestore;
  final FirebaseAuth firebaseAuth;

  CardsService({
    required this.firestore,
    required this.firebaseAuth,
  });

  Future addCards(CardModel card) {
    final cardsRef = firestore
        .collection(FirebaseConstants.usersCollection)
        .doc(firebaseAuth.currentUser!.uid)
        .collection(FirebaseConstants.cardsCollection);
    return cardsRef.add(card.toJson());
  }

  Stream<List<CardModel>> cards() {
    return firestore
        .collection(
          '${FirebaseConstants.usersCollection}/${firebaseAuth.currentUser!.uid}/${FirebaseConstants.cardsCollection}',
        )
        .snapshots()
        .map(
      (event) {
        List<CardModel> cards = [];
        for (var doc in event.docs) {
          final data = doc.data() as Map<String, dynamic>;
          data['id'] = doc.id;
          cards.add(CardModel.fromJson(data));
        }
        return cards;
      },
    );
  }

  Stream<CardModel?> cardById(String id) {
    return firestore
        .collection(
          '${FirebaseConstants.usersCollection}/${firebaseAuth.currentUser!.uid}/${FirebaseConstants.cardsCollection}',
        )
        .doc(id)
        .snapshots()
        .map(
      (snapshot) {
        final data = snapshot.data();
        if (data != null) {
          data['id'] = snapshot.id;
          return CardModel.fromJson(data);
        } else {
          return null;
        }
      },
    );
  }

  Future<void> removeCardById(CardModel card) {
    return firestore
        .collection(
          '${FirebaseConstants.usersCollection}/${firebaseAuth.currentUser!.uid}/${FirebaseConstants.cardsCollection}',
        )
        .doc(card.id)
        .update(card.toJson());
  }

  Future<void> updateCardById(CardModel card) {
    return firestore
        .collection(
          '${FirebaseConstants.usersCollection}/${firebaseAuth.currentUser!.uid}/${FirebaseConstants.cardsCollection}',
        )
        .doc(card.id)
        .update(card.toJson());
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_stream.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$getCardsHash() => r'142f2c0dc469e320aaf44d231f04d787a512d3e1';

/// See also [getCards].
@ProviderFor(getCards)
final getCardsProvider = AutoDisposeStreamProvider<List<CardModel>>.internal(
  getCards,
  name: r'getCardsProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$getCardsHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef GetCardsRef = AutoDisposeStreamProviderRef<List<CardModel>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member

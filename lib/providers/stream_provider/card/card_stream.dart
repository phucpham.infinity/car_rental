// ignore_for_file: unnecessary_cast

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rental_car/models/card/card_model.dart';
import 'package:rental_car/providers/services_provider/cards/cards_services.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'card_stream.g.dart';

@riverpod
Stream<List<CardModel>> getCards(Ref ref) {
  return ref.watch(cardsServiceProvider).cards();
}

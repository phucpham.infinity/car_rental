import 'package:auto_route/auto_route.dart';
import 'package:rental_car/common/page_route.dart';

import 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType => const RouteType.custom();

  @override
  final List<CustomRoute> routes = [
    CustomPageRoute(page: SplashWidget.page, path: '/', initial: true),
    CustomPageRoute(page: WelcomeWidget.page, path: '/welcome'),
    CustomPageRoute(page: LoginPhoneWidget.page, path: '/login-phone'),
    CustomPageRoute(page: LoginOtp.page, path: '/login-otp'),
    CustomPageRoute(page: HomeRoute.page, path: '/home'),
    CustomPageRoute(page: Register.page, path: '/register'),
    CustomPageRoute(page: PaymentMethodRoute.page, path: '/payment-method'),
    CustomPageRoute(page: AddCardRoute.page, path: '/payment-method/add-card'),
  ];
}

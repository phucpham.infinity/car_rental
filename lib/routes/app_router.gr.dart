// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i10;
import 'package:flutter/material.dart' as _i11;
import 'package:rental_car/screens/auth/login_otp/login_otp.dart' as _i3;
import 'package:rental_car/screens/auth/login_phone/login_phone.dart' as _i5;
import 'package:rental_car/screens/auth/otp_login/otp_screen.dart' as _i4;
import 'package:rental_car/screens/auth/register/register.dart' as _i7;
import 'package:rental_car/screens/auth/wellcome/wellcome_screen.dart' as _i9;
import 'package:rental_car/screens/home/home_screen.dart' as _i2;
import 'package:rental_car/screens/payment_method/add_card/add_card.screen.dart'
    as _i1;
import 'package:rental_car/screens/payment_method/payment_method.screen.dart'
    as _i6;
import 'package:rental_car/screens/splash/splash_screen.dart' as _i8;

abstract class $AppRouter extends _i10.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i10.PageFactory> pagesMap = {
    AddCardRoute.name: (routeData) {
      final args = routeData.argsAs<AddCardRouteArgs>(
          orElse: () => const AddCardRouteArgs());
      return _i10.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i1.AddCardScreen(key: args.key),
      );
    },
    HomeRoute.name: (routeData) {
      return _i10.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.HomeScreen(),
      );
    },
    LoginOtp.name: (routeData) {
      return _i10.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i3.LoginOtp(),
      );
    },
    LoginPhoneOtpWidget.name: (routeData) {
      return _i10.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i4.LoginPhoneOtpWidget(),
      );
    },
    LoginPhoneWidget.name: (routeData) {
      final args = routeData.argsAs<LoginPhoneWidgetArgs>(
          orElse: () => const LoginPhoneWidgetArgs());
      return _i10.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i5.LoginPhoneWidget(key: args.key),
      );
    },
    PaymentMethodRoute.name: (routeData) {
      final args = routeData.argsAs<PaymentMethodRouteArgs>(
          orElse: () => const PaymentMethodRouteArgs());
      return _i10.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i6.PaymentMethodScreen(key: args.key),
      );
    },
    Register.name: (routeData) {
      final args =
          routeData.argsAs<RegisterArgs>(orElse: () => const RegisterArgs());
      return _i10.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i7.Register(key: args.key),
      );
    },
    SplashWidget.name: (routeData) {
      return _i10.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i8.SplashWidget(),
      );
    },
    WelcomeWidget.name: (routeData) {
      return _i10.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i9.WelcomeWidget(),
      );
    },
  };
}

/// generated route for
/// [_i1.AddCardScreen]
class AddCardRoute extends _i10.PageRouteInfo<AddCardRouteArgs> {
  AddCardRoute({
    _i11.Key? key,
    List<_i10.PageRouteInfo>? children,
  }) : super(
          AddCardRoute.name,
          args: AddCardRouteArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'AddCardRoute';

  static const _i10.PageInfo<AddCardRouteArgs> page =
      _i10.PageInfo<AddCardRouteArgs>(name);
}

class AddCardRouteArgs {
  const AddCardRouteArgs({this.key});

  final _i11.Key? key;

  @override
  String toString() {
    return 'AddCardRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i2.HomeScreen]
class HomeRoute extends _i10.PageRouteInfo<void> {
  const HomeRoute({List<_i10.PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const _i10.PageInfo<void> page = _i10.PageInfo<void>(name);
}

/// generated route for
/// [_i3.LoginOtp]
class LoginOtp extends _i10.PageRouteInfo<void> {
  const LoginOtp({List<_i10.PageRouteInfo>? children})
      : super(
          LoginOtp.name,
          initialChildren: children,
        );

  static const String name = 'LoginOtp';

  static const _i10.PageInfo<void> page = _i10.PageInfo<void>(name);
}

/// generated route for
/// [_i4.LoginPhoneOtpWidget]
class LoginPhoneOtpWidget extends _i10.PageRouteInfo<void> {
  const LoginPhoneOtpWidget({List<_i10.PageRouteInfo>? children})
      : super(
          LoginPhoneOtpWidget.name,
          initialChildren: children,
        );

  static const String name = 'LoginPhoneOtpWidget';

  static const _i10.PageInfo<void> page = _i10.PageInfo<void>(name);
}

/// generated route for
/// [_i5.LoginPhoneWidget]
class LoginPhoneWidget extends _i10.PageRouteInfo<LoginPhoneWidgetArgs> {
  LoginPhoneWidget({
    _i11.Key? key,
    List<_i10.PageRouteInfo>? children,
  }) : super(
          LoginPhoneWidget.name,
          args: LoginPhoneWidgetArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'LoginPhoneWidget';

  static const _i10.PageInfo<LoginPhoneWidgetArgs> page =
      _i10.PageInfo<LoginPhoneWidgetArgs>(name);
}

class LoginPhoneWidgetArgs {
  const LoginPhoneWidgetArgs({this.key});

  final _i11.Key? key;

  @override
  String toString() {
    return 'LoginPhoneWidgetArgs{key: $key}';
  }
}

/// generated route for
/// [_i6.PaymentMethodScreen]
class PaymentMethodRoute extends _i10.PageRouteInfo<PaymentMethodRouteArgs> {
  PaymentMethodRoute({
    _i11.Key? key,
    List<_i10.PageRouteInfo>? children,
  }) : super(
          PaymentMethodRoute.name,
          args: PaymentMethodRouteArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'PaymentMethodRoute';

  static const _i10.PageInfo<PaymentMethodRouteArgs> page =
      _i10.PageInfo<PaymentMethodRouteArgs>(name);
}

class PaymentMethodRouteArgs {
  const PaymentMethodRouteArgs({this.key});

  final _i11.Key? key;

  @override
  String toString() {
    return 'PaymentMethodRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i7.Register]
class Register extends _i10.PageRouteInfo<RegisterArgs> {
  Register({
    _i11.Key? key,
    List<_i10.PageRouteInfo>? children,
  }) : super(
          Register.name,
          args: RegisterArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'Register';

  static const _i10.PageInfo<RegisterArgs> page =
      _i10.PageInfo<RegisterArgs>(name);
}

class RegisterArgs {
  const RegisterArgs({this.key});

  final _i11.Key? key;

  @override
  String toString() {
    return 'RegisterArgs{key: $key}';
  }
}

/// generated route for
/// [_i8.SplashWidget]
class SplashWidget extends _i10.PageRouteInfo<void> {
  const SplashWidget({List<_i10.PageRouteInfo>? children})
      : super(
          SplashWidget.name,
          initialChildren: children,
        );

  static const String name = 'SplashWidget';

  static const _i10.PageInfo<void> page = _i10.PageInfo<void>(name);
}

/// generated route for
/// [_i9.WelcomeWidget]
class WelcomeWidget extends _i10.PageRouteInfo<void> {
  const WelcomeWidget({List<_i10.PageRouteInfo>? children})
      : super(
          WelcomeWidget.name,
          initialChildren: children,
        );

  static const String name = 'WelcomeWidget';

  static const _i10.PageInfo<void> page = _i10.PageInfo<void>(name);
}

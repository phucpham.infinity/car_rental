import 'package:flutter/material.dart';
import 'package:flutterflow_ui/flutterflow_ui.dart';
import 'package:rental_car/theme/app_theme.dart';

class HeaderMain extends StatelessWidget {
  final dynamic Function()? onOpenDrawer;
  const HeaderMain({super.key, this.onOpenDrawer});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(2, 0, 0, 0),
      child: Container(
        width: MediaQuery.sizeOf(context).width,
        height: 67,
        decoration: BoxDecoration(
          color: AppTheme.of(context).secondaryBackground,
          boxShadow: const [
            BoxShadow(
              blurRadius: 4,
              color: Color(0x33000000),
              offset: Offset(0, 2),
            )
          ],
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
            topLeft: Radius.circular(0),
            topRight: Radius.circular(0),
          ),
        ),
        child: Padding(
          padding: EdgeInsetsDirectional.fromSTEB(10, 0, 20, 0),
          child: Stack(
            children: [
              Align(
                alignment: const AlignmentDirectional(0, 0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Switch.adaptive(
                      value: true,
                      onChanged: (newValue) async {},
                      activeColor: AppTheme.of(context).primary,
                      activeTrackColor: AppTheme.of(context).accent1,
                      inactiveTrackColor: AppTheme.of(context).alternate,
                      inactiveThumbColor: AppTheme.of(context).secondaryText,
                    ),
                  ],
                ),
              ),
              Align(
                alignment: const AlignmentDirectional(1, 0),
                child: Material(
                  color: Colors.transparent,
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(24),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(24),
                      border: Border.all(
                        color: AppTheme.of(context).primaryLight,
                        width: 2,
                      ),
                    ),
                    child: Container(
                      width: 40,
                      height: 40,
                      clipBehavior: Clip.antiAlias,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: Image.network(
                        'https://picsum.photos/seed/108/600',
                        width: 40,
                        height: 40,
                        fit: BoxFit.fill,
                      ),
                      // child: CachedNetworkImage(
                      //   fadeInDuration: const Duration(milliseconds: 3000),
                      //   fadeOutDuration: const Duration(milliseconds: 3000),
                      //   imageUrl: 'https://picsum.photos/seed/108/600',
                      //   fit: BoxFit.cover,
                      // ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: const AlignmentDirectional(-1, 0),
                child: FlutterFlowIconButton(
                  borderRadius: 50,
                  borderWidth: 0,
                  buttonSize: 50,
                  icon: Icon(
                    Icons.menu,
                    color: AppTheme.of(context).primaryText,
                    size: 32,
                  ),
                  onPressed: onOpenDrawer,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

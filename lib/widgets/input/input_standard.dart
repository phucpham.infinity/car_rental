import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rental_car/theme/app_theme.dart';

class InputStandardWidget extends StatelessWidget {
  final String? initialValue;
  final FormFieldSetter<String>? onSaved;
  final FormFieldValidator<String>? validator;
  final ValueChanged<String>? onChanged;
  final bool obscureText;
  final bool autofocus;
  final Widget? icon;
  final TextInputType? keyboardType;
  final TextEditingController? controller;
  final String? labelText;
  final int? maxLength;

  const InputStandardWidget({
    super.key,
    this.initialValue,
    this.onSaved,
    this.validator,
    this.onChanged,
    this.obscureText = false,
    this.keyboardType,
    this.controller,
    this.labelText,
    this.autofocus = false,
    this.icon,
    this.maxLength,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      autofocus: autofocus,
      obscureText: obscureText,
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: GoogleFonts.getFont(
          'Lato',
          color: AppTheme.of(context).secondaryText,
          fontSize: 16,
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: AppTheme.of(context).primaryDark,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(0),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: AppTheme.of(context).primaryDark,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(0),
        ),
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: AppTheme.of(context).error,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(0),
        ),
        focusedErrorBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: AppTheme.of(context).error,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(0),
        ),
        prefixIcon: icon,
      ),
      style: GoogleFonts.getFont(
        'Lato',
        color: AppTheme.of(context).primaryDark,
        fontSize: 16,
      ),
      keyboardType: keyboardType,
      validator: validator,
      maxLength: maxLength,
    );
  }
}

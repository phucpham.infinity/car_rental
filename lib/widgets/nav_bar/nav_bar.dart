// ignore_for_file: unnecessary_brace_in_string_interps

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rental_car/providers/app_provider/authentication/authentication_bloc.dart';

class NavBar extends ConsumerWidget {
  const NavBar({super.key});

  @override
  Widget build(BuildContext context, ref) {
    final phoneNumber = ref.watch(authenticationBlocProvider).user?.phoneNumber;
    final displayName = ref.watch(authenticationBlocProvider).user?.displayName;

    final status = ref.watch(authenticationBlocProvider).status;
    switch (status) {
      case AUTHENTICATION_STATUS.CHECKING:
        context.router.pushNamed('/');
        break;
      default:
    }

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('$displayName'),
            accountEmail: Text('${phoneNumber}'),
          ),
          ListTile(
            leading: const Icon(Icons.library_books),
            title: const Text('Books'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.payments_sharp),
            title: const Text('Payment Method'),
            onTap: () {
              context.router.pushNamed('/payment-method');
            },
          ),
          ListTile(
            leading: const Icon(Icons.account_circle),
            title: const Text('Profile'),
            onTap: () {},
          ),
          const Divider(),
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Setting'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.logout_outlined),
            title: const Text('Logout'),
            onTap: () async {
              ref.read(authenticationBlocProvider.notifier).logout();
            },
          )
        ],
      ),
    );
  }
}

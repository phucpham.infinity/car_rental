import 'package:flutter/material.dart';
import 'package:flutterflow_ui/flutterflow_ui.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rental_car/theme/app_theme.dart';

class BtnFilledWidget extends StatelessWidget {
  final String label;
  final dynamic Function()? onPressed;
  final bool isLoading;
  const BtnFilledWidget({
    super.key,
    required this.label,
    this.onPressed,
    this.isLoading = false,
  });

  @override
  Widget build(BuildContext context) {
    return FFButtonWidget(
      onPressed: onPressed,
      text: label,
      showLoadingIndicator: isLoading,
      options: FFButtonOptions(
        width: MediaQuery.sizeOf(context).width,
        height: 60,
        padding: const EdgeInsetsDirectional.fromSTEB(24, 0, 24, 0),
        iconPadding: const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
        color: AppTheme.of(context).primaryDark,
        textStyle: GoogleFonts.getFont(
          'Lato',
          color: AppTheme.of(context).primaryLight,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
        elevation: 0,
        borderSide: const BorderSide(
          color: Colors.transparent,
          width: 0,
        ),
        borderRadius: BorderRadius.circular(60),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rental_car/theme/app_theme.dart';

class HeaderBarWidget extends StatelessWidget {
  final String title;
  final dynamic Function()? onTap;
  const HeaderBarWidget({super.key, required this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: const AlignmentDirectional(0, 0),
      child: Stack(
        alignment: const AlignmentDirectional(-1, 0),
        children: [
          InkWell(
            splashColor: Colors.transparent,
            focusColor: Colors.transparent,
            hoverColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: onTap,
            child: Icon(
              Icons.chevron_left,
              color: AppTheme.of(context).secondaryText,
              size: 32,
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Align(
                alignment: const AlignmentDirectional(0, 0),
                child: Text(
                  title,
                  style: GoogleFonts.getFont(
                    'Lato',
                    color: AppTheme.of(context).primaryDark,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

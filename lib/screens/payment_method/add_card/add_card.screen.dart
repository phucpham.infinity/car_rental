// ignore_for_file: deprecated_member_use, use_super_parameters

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutterflow_ui/flutterflow_ui.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rental_car/helper/validator.dart';
import 'package:rental_car/screens/payment_method/bloc/card_bloc.dart';
import 'package:rental_car/theme/app_theme.dart';
import 'package:rental_car/widgets/button/btn_outline.dart';
import 'package:rental_car/widgets/header_bar/header_bar_widget.dart';
import 'package:rental_car/widgets/input/input_standard.dart';

@RoutePage()
class AddCardScreen extends ConsumerWidget {
  AddCardScreen({Key? key}) : super(key: key);
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  final TextEditingController fullNameController =
      TextEditingController(text: "");
  final TextEditingController cardNumberController =
      TextEditingController(text: "");
  final TextEditingController expController = TextEditingController(text: "");

  final animationsMap = {
    'containerOnPageLoadAnimation': AnimationInfo(
      trigger: AnimationTrigger.onPageLoad,
      effects: [
        MoveEffect(
          curve: Curves.easeInOut,
          delay: 0.ms,
          duration: 600.ms,
          begin: const Offset(0, 33),
          end: const Offset(0, 0),
        ),
      ],
    ),
  };

  @override
  Widget build(BuildContext context, ref) {
    final isLoadingAddNew = ref.watch(cardBlocProvider).isLoadingAddNewCard;

    return GestureDetector(
      onTap: () => {},
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          key: scaffoldKey,
          backgroundColor: AppTheme.of(context).primaryLight,
          body: SafeArea(
            top: true,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 40),
                  child: HeaderBarWidget(
                    title: 'PAYMENT METHOD',
                    onTap: () {
                      context.router.back();
                    },
                  ),
                ),
                Expanded(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      color: AppTheme.of(context).secondaryBackground,
                      boxShadow: const [
                        BoxShadow(
                          blurRadius: 4,
                          color: Color(0x33000000),
                          offset: Offset(0, -2),
                        )
                      ],
                      borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(0),
                        bottomRight: Radius.circular(0),
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    child: Padding(
                      padding:
                          const EdgeInsetsDirectional.fromSTEB(20, 18, 20, 18),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                0, 0, 0, 24),
                            child: Text(
                              'Add New Card',
                              style: GoogleFonts.getFont(
                                'Lato',
                                color: AppTheme.of(context).primaryDark,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                          ),
                          Expanded(
                            child: SingleChildScrollView(
                              child: Form(
                                key: formKey,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    InputStandardWidget(
                                      labelText: 'Full Name',
                                      controller: fullNameController,
                                      icon: const Icon(Icons.person_outline),
                                      validator: (value) =>
                                          validatorFieldEmpty(value),
                                    ),
                                    InputStandardWidget(
                                      labelText: 'Card Number',
                                      controller: cardNumberController,
                                      icon: const Icon(Icons.card_membership),
                                      validator: (value) =>
                                          validatorFieldEmpty(value),
                                    ),
                                    InputStandardWidget(
                                      labelText: 'Exp.',
                                      controller: expController,
                                      icon: const Icon(Icons.date_range),
                                      validator: (value) =>
                                          validatorFieldEmpty(value),
                                    ),
                                  ].divide(const SizedBox(height: 20)),
                                ),
                              ),
                            ),
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: BtnOutlineWidget(
                                  label: 'Add Card',
                                  isLoading: isLoadingAddNew,
                                  onPressed: () async {
                                    if (formKey.currentState!.validate()) {
                                      ref
                                          .read(cardBlocProvider.notifier)
                                          .addNewCard(
                                            context: context,
                                            fullName:
                                                fullNameController.text.trim(),
                                            cardNumber: cardNumberController
                                                .text
                                                .trim(),
                                            exp: expController.text.trim(),
                                          )
                                          .fold(
                                            (l) => {},
                                            (r) => context.router.back(),
                                          );
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ).animateOnPageLoad(
                      animationsMap['containerOnPageLoadAnimation']!),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

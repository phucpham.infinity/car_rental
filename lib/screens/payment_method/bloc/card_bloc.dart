// ignore_for_file: constant_identifier_names, camel_case_types, no_leading_underscores_for_local_identifiers, use_build_context_synchronously, non_constant_identifier_names

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fpdart/fpdart.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rental_car/helper/failure.dart';
import 'package:rental_car/helper/show_snack_bar.dart';
import 'package:rental_car/models/card/card_model.dart';
import 'package:rental_car/providers/services_provider/cards/cards_services.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:uuid/uuid.dart';

part 'card_bloc.g.dart';
part 'card_bloc.freezed.dart';

enum CARD_BLOC_STATUS { LOADING, INIT }

@freezed
class CardBlocState with _$CardBlocState {
  const factory CardBlocState({
    required CARD_BLOC_STATUS status,
    required bool isLoadingAddNewCard,
  }) = _CardBlocState;
}

@riverpod
class CardBloc extends _$CardBloc {
  @override
  CardBlocState build() {
    return const CardBlocState(
      status: CARD_BLOC_STATUS.INIT,
      isLoadingAddNewCard: false,
    );
  }

  Either<dynamic, Future<dynamic>> addNewCard({
    required BuildContext context,
    required String fullName,
    required String cardNumber,
    required String exp,
  }) {
    state = state.copyWith(isLoadingAddNewCard: true);
    const uuid = Uuid();
    final card = CardModel(
      id: uuid.v4(),
      cardNumber: cardNumber,
      fullName: fullName,
      exp: exp,
      type: 'master-card',
    );
    try {
      final res = ref.read(cardsServiceProvider).addCards(card);
      showSnackBar(
        context: context,
        content: 'Add done!',
        type: SnackBarType.success,
      );
      return right(res);
    } on FirebaseException catch (e) {
      showSnackBar(
        context: context,
        content: 'Error: ${e.message!}',
        type: SnackBarType.error,
      );
      return left(e.message!);
    } catch (e) {
      showSnackBar(
        context: context,
        content: 'Error: ${e.toString()}',
        type: SnackBarType.error,
      );
      return left(Failure(message: e.toString()));
    } finally {
      state = state.copyWith(isLoadingAddNewCard: false);
    }
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_bloc.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$cardBlocHash() => r'900e886e739bc4d32aaec89793151167188ad1df';

/// See also [CardBloc].
@ProviderFor(CardBloc)
final cardBlocProvider =
    AutoDisposeNotifierProvider<CardBloc, CardBlocState>.internal(
  CardBloc.new,
  name: r'cardBlocProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$cardBlocHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$CardBloc = AutoDisposeNotifier<CardBlocState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member

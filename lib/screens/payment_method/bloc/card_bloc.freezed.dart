// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'card_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CardBlocState {
  CARD_BLOC_STATUS get status => throw _privateConstructorUsedError;
  bool get isLoadingAddNewCard => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CardBlocStateCopyWith<CardBlocState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CardBlocStateCopyWith<$Res> {
  factory $CardBlocStateCopyWith(
          CardBlocState value, $Res Function(CardBlocState) then) =
      _$CardBlocStateCopyWithImpl<$Res, CardBlocState>;
  @useResult
  $Res call({CARD_BLOC_STATUS status, bool isLoadingAddNewCard});
}

/// @nodoc
class _$CardBlocStateCopyWithImpl<$Res, $Val extends CardBlocState>
    implements $CardBlocStateCopyWith<$Res> {
  _$CardBlocStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? isLoadingAddNewCard = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as CARD_BLOC_STATUS,
      isLoadingAddNewCard: null == isLoadingAddNewCard
          ? _value.isLoadingAddNewCard
          : isLoadingAddNewCard // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CardBlocStateImplCopyWith<$Res>
    implements $CardBlocStateCopyWith<$Res> {
  factory _$$CardBlocStateImplCopyWith(
          _$CardBlocStateImpl value, $Res Function(_$CardBlocStateImpl) then) =
      __$$CardBlocStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({CARD_BLOC_STATUS status, bool isLoadingAddNewCard});
}

/// @nodoc
class __$$CardBlocStateImplCopyWithImpl<$Res>
    extends _$CardBlocStateCopyWithImpl<$Res, _$CardBlocStateImpl>
    implements _$$CardBlocStateImplCopyWith<$Res> {
  __$$CardBlocStateImplCopyWithImpl(
      _$CardBlocStateImpl _value, $Res Function(_$CardBlocStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? isLoadingAddNewCard = null,
  }) {
    return _then(_$CardBlocStateImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as CARD_BLOC_STATUS,
      isLoadingAddNewCard: null == isLoadingAddNewCard
          ? _value.isLoadingAddNewCard
          : isLoadingAddNewCard // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$CardBlocStateImpl implements _CardBlocState {
  const _$CardBlocStateImpl(
      {required this.status, required this.isLoadingAddNewCard});

  @override
  final CARD_BLOC_STATUS status;
  @override
  final bool isLoadingAddNewCard;

  @override
  String toString() {
    return 'CardBlocState(status: $status, isLoadingAddNewCard: $isLoadingAddNewCard)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CardBlocStateImpl &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.isLoadingAddNewCard, isLoadingAddNewCard) ||
                other.isLoadingAddNewCard == isLoadingAddNewCard));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status, isLoadingAddNewCard);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CardBlocStateImplCopyWith<_$CardBlocStateImpl> get copyWith =>
      __$$CardBlocStateImplCopyWithImpl<_$CardBlocStateImpl>(this, _$identity);
}

abstract class _CardBlocState implements CardBlocState {
  const factory _CardBlocState(
      {required final CARD_BLOC_STATUS status,
      required final bool isLoadingAddNewCard}) = _$CardBlocStateImpl;

  @override
  CARD_BLOC_STATUS get status;
  @override
  bool get isLoadingAddNewCard;
  @override
  @JsonKey(ignore: true)
  _$$CardBlocStateImplCopyWith<_$CardBlocStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

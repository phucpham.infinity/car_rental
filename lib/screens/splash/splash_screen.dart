// ignore_for_file: constant_pattern_never_matches_value_type

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutterflow_ui/flutterflow_ui.dart';
import 'package:rental_car/providers/app_provider/authentication/authentication_bloc.dart';

@RoutePage()
class SplashWidget extends ConsumerStatefulWidget {
  const SplashWidget({super.key});

  @override
  SplashWidgetState createState() => SplashWidgetState();
}

class SplashWidgetState extends ConsumerState<SplashWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    ref.read(authenticationBlocProvider.notifier).checkAuth();
  }

  @override
  Widget build(BuildContext context) {
    final status = ref.watch(authenticationBlocProvider).status;

    switch (status) {
      case AUTHENTICATION_STATUS.NOT_REGISTER:
        context.router.pushNamed('/welcome');
        break;
      case AUTHENTICATION_STATUS.REGISTERED:
        context.router.pushNamed('/home');
        break;
      default:
        print('status $status');
    }

    return GestureDetector(
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: FlutterFlowTheme.of(context).primaryBackground,
        body: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  color: FlutterFlowTheme.of(context).secondaryBackground,
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: Image.asset(
                      'assets/images/splash-screen_bg.png',
                    ).image,
                  ),
                ),
                child: Align(
                  alignment: const AlignmentDirectional(0, 0),
                  child: Stack(
                    alignment: const AlignmentDirectional(0, 0),
                    children: [
                      Align(
                        alignment: const AlignmentDirectional(1, 0),
                        child: Padding(
                          padding:
                              const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 30),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.asset(
                              'assets/images/splash-screen_img1.png',
                              width: 424,
                              height: 1006,
                              fit: BoxFit.none,
                              alignment: const Alignment(1, 0),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: const AlignmentDirectional(0, 0),
                        child: Padding(
                          padding:
                              const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 30),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.asset(
                              'assets/images/full_logo.png',
                              width: 300,
                              height: 85,
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

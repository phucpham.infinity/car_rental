// ignore_for_file: camel_case_types, constant_identifier_names, no_leading_underscores_for_local_identifiers

import 'package:firebase_auth/firebase_auth.dart';
import 'package:rental_car/helper/validator.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_bloc.g.dart';
part 'auth_bloc.freezed.dart';

enum AUTH_STATUS {
  INIT,
  VERIFICATION,
  LOADING,
  OTP_ERROR,
  OTP_DONE,
  LOGIN_ERROR,
  LOGIN_DONE
}

@freezed
class AuthState with _$AuthState {
  const factory AuthState({
    required AUTH_STATUS status,
    required String verificationId,
    required String error,
    required String phone,
  }) = _AuthState;
}

@riverpod
class AuthBloc extends _$AuthBloc {
  static final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  @override
  AuthState build() {
    return const AuthState(
      status: AUTH_STATUS.INIT,
      verificationId: "",
      error: "",
      phone: "",
    );
  }

  sendOtp({required String phone}) async {
    state = state.copyWith(
      status: AUTH_STATUS.LOADING,
      phone: phone,
    );

    final _phone = extractPhoneNumber(phone);

    await _firebaseAuth
        .verifyPhoneNumber(
      phoneNumber: '+84$_phone',
      timeout: const Duration(seconds: 30),
      verificationCompleted: (phoneAuthCredential) async {
        return;
      },
      verificationFailed: (err) async {
        state = state.copyWith(
          status: AUTH_STATUS.OTP_ERROR,
          error: err.message.toString(),
        );
      },
      codeSent: (verificationId, codeToken) async {
        state = state.copyWith(
          status: AUTH_STATUS.OTP_DONE,
          verificationId: verificationId,
        );
      },
      codeAutoRetrievalTimeout: (verificationId) async {
        return;
      },
    )
        .onError((error, stackTrace) {
      state = state.copyWith(
          status: AUTH_STATUS.OTP_ERROR, error: error.toString());
    });
  }

  loginWithOtp({required String smsCode}) async {
    state = state.copyWith(status: AUTH_STATUS.LOADING);
    final credential = PhoneAuthProvider.credential(
      verificationId: state.verificationId,
      smsCode: smsCode,
    );
    try {
      final user = await _firebaseAuth.signInWithCredential(credential);
      state = state.copyWith(status: AUTH_STATUS.LOGIN_DONE, phone: '');
      if (user.user != null) {
        return "Success";
      } else {
        return "Error in Otp login";
      }
    } on FirebaseAuthException catch (e) {
      state = state.copyWith(
        status: AUTH_STATUS.LOGIN_ERROR,
        error: e.message.toString(),
      );
    } catch (e) {
      state = state.copyWith(
        status: AUTH_STATUS.LOGIN_ERROR,
        error: e.toString(),
      );
    }
  }

  Future logout() async {
    await _firebaseAuth.signOut();
  }

  Future<bool> isLoggedIn() async {
    var user = _firebaseAuth.currentUser;
    return user != null;
  }
}

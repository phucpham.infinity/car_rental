// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_bloc.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authBlocHash() => r'9d170e06cd7ac28bd8bd16b183dbb0b48b0a6b86';

/// See also [AuthBloc].
@ProviderFor(AuthBloc)
final authBlocProvider =
    AutoDisposeNotifierProvider<AuthBloc, AuthState>.internal(
  AuthBloc.new,
  name: r'authBlocProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$authBlocHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AuthBloc = AutoDisposeNotifier<AuthState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member

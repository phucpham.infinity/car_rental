import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:rental_car/helper/show_snack_bar.dart';
import 'package:rental_car/helper/validator.dart';
import 'package:rental_car/screens/auth/bloc/auth_bloc.dart';
import 'package:rental_car/theme/app_theme.dart';
import 'package:rental_car/widgets/button/btn_filled.dart';
import 'package:rental_car/widgets/header_bar/header_bar_widget.dart';
import 'package:rental_car/widgets/input/input_standard.dart';

@RoutePage()
class LoginPhoneWidget extends ConsumerWidget {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<ScaffoldState>();

  LoginPhoneWidget({super.key});

  @override
  Widget build(BuildContext context, ref) {
    final status = ref.watch(authBlocProvider).status;
    switch (status) {
      case AUTH_STATUS.OTP_DONE:
        context.router.pushNamed('/login-otp');
        break;
      case AUTH_STATUS.OTP_ERROR:
        showSnackBar(
          context: context,
          content: ref.watch(authBlocProvider).error,
        );
        break;
      default:
    }

    final formKey = GlobalKey<FormState>();
    final phoneController = TextEditingController(
        text: ref.read(authBlocProvider).phone.toString());

    return GestureDetector(
      onTap: () {},
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: AppTheme.of(context).lightGreen,
        body: SafeArea(
          top: true,
          child: Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Expanded(
                      child: Align(
                        alignment: AlignmentDirectional(0, 0),
                        child: HeaderBarWidget(
                          title: 'LOGIN',
                          onTap: () {
                            context.router.pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 100),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image.asset(
                          'assets/images/logo_sm.png',
                          width: 300,
                          height: 71,
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Padding(
                            padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
                            child: Text(
                              'Enter mobile number',
                              style: GoogleFonts.getFont(
                                'Lato',
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Padding(
                              padding: const EdgeInsetsDirectional.fromSTEB(
                                  0, 10, 0, 0),
                              child: Form(
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                key: formKey,
                                child: InputStandardWidget(
                                  controller: phoneController,
                                  validator: (value) =>
                                      validatorFieldPhone(value),
                                  autofocus: true,
                                  keyboardType: TextInputType.phone,
                                  icon: Icon(Icons.phone_iphone),
                                ),
                              )),
                        ),
                      ],
                    ),
                  ],
                ),
                const Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [],
                  ),
                ),
                Consumer(builder: (context, ref, widget) {
                  return Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 20),
                        child: BtnFilledWidget(
                          label: 'Send OTP',
                          isLoading: ref.watch(authBlocProvider).status ==
                              AUTH_STATUS.LOADING,
                          onPressed: () {
                            if (formKey.currentState!.validate()) {
                              ref
                                  .watch(authBlocProvider.notifier)
                                  .sendOtp(phone: phoneController.text.trim());
                            }
                          },
                        ),
                      ),
                    ],
                  );
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'otp_bloc.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$otpBlocHash() => r'aa787ac83aaaa21edd6e4d400a9acb6d57507787';

/// See also [OtpBloc].
@ProviderFor(OtpBloc)
final otpBlocProvider = AutoDisposeNotifierProvider<OtpBloc, OtpModel>.internal(
  OtpBloc.new,
  name: r'otpBlocProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$otpBlocHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$OtpBloc = AutoDisposeNotifier<OtpModel>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member

import 'package:rental_car/screens/auth/otp_login/modal/otp_modal.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'otp_bloc.g.dart';

@riverpod
class OtpBloc extends _$OtpBloc {
  @override
  OtpModel build() {
    return const OtpModel(
      phone: '',
    );
  }
}

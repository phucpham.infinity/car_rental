import 'package:freezed_annotation/freezed_annotation.dart';

part 'otp_modal.freezed.dart';

@freezed
class OtpModel with _$OtpModel {
  const factory OtpModel({
    required String phone,
  }) = _OtpModel;
}

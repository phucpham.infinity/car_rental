import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rental_car/helper/show_snack_bar.dart';
import 'package:rental_car/helper/validator.dart';
import 'package:rental_car/screens/auth/bloc/auth_bloc.dart';
import 'package:rental_car/theme/app_theme.dart';
import 'package:rental_car/widgets/button/btn_filled.dart';
import 'package:rental_car/widgets/header_bar/header_bar_widget.dart';
import 'package:rental_car/widgets/input/input_standard.dart';

@RoutePage()
class LoginOtp extends ConsumerWidget {
  const LoginOtp({super.key});

  @override
  Widget build(BuildContext context, ref) {
    final scaffoldKey = GlobalKey<ScaffoldState>();
    final formKey = GlobalKey<FormState>();

    final status = ref.watch(authBlocProvider).status;
    switch (status) {
      case AUTH_STATUS.LOGIN_DONE:
        context.router.pushNamed('/home');
        break;
      case AUTH_STATUS.LOGIN_ERROR:
        showSnackBar(
          context: context,
          content: ref.watch(authBlocProvider).error,
        );
        break;
      default:
    }

    final TextEditingController inputOTPController =
        TextEditingController(text: "");
    final queryParams = context.routeData.queryParams;

    return GestureDetector(
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: AppTheme.of(context).lightGreen,
        body: SafeArea(
          top: true,
          child: Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Expanded(
                      child: Align(
                        alignment: const AlignmentDirectional(0, 0),
                        child: HeaderBarWidget(
                          title: 'VERIFICATION',
                          onTap: () {
                            context.router.pop();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 100),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image.asset(
                          'assets/images/logo_sm.png',
                          width: 300,
                          height: 92,
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                0, 0, 0, 10),
                            child: Text(
                              'Verify mobile number',
                              style: GoogleFonts.getFont(
                                'Lato',
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsetsDirectional.fromSTEB(
                                0, 0, 0, 40),
                            child: Text(
                              'Check your SMS messages. We’ve sent you \nthe code at ${queryParams.optString("phone")}',
                              style: GoogleFonts.getFont(
                                'Lato',
                                fontWeight: FontWeight.normal,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Form(
                            key: formKey,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: InputStandardWidget(
                              keyboardType: TextInputType.number,
                              validator: (value) => validatorFieldEmpty(value),
                              autofocus: true,
                              icon: const Icon(Icons.numbers),
                              controller: inputOTPController,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Consumer(builder: (context, ref, widget) {
                        return BtnFilledWidget(
                          label: 'Verify',
                          isLoading: ref.watch(authBlocProvider).status ==
                              AUTH_STATUS.LOADING,
                          onPressed: () {
                            ref.watch(authBlocProvider.notifier).loginWithOtp(
                                smsCode: inputOTPController.text.trim());
                          },
                        );
                      }),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

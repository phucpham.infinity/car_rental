// ignore_for_file: unnecessary_new

validatorFieldEmpty(value) {
  if (value == null || value.isEmpty) {
    return 'Please enter some text';
  }
  return null;
}

validatorFieldPhone(value) {
  Pattern pattern = r'^(?:\+84|0)[3-9][0-9]{8}$';
  RegExp regex = new RegExp(pattern.toString());
  if (!regex.hasMatch(value!)) {
    return 'Please enter a valid phone number';
  } else {
    return null;
  }
}

String extractPhoneNumber(String input) {
  Pattern pattern = r'^(?:\+84|0)?([3-9][0-9]{8})$';
  RegExp regex = new RegExp(pattern.toString());
  var match = regex.firstMatch(input);
  return match != null ? match.group(1)! : input;
}

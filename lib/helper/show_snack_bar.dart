// ignore_for_file: file_names, unnecessary_import

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

enum SnackBarType { error, info, success }

showSnackBar({
  required BuildContext context,
  required String content,
  SnackBarType type = SnackBarType.info,
}) {
  WidgetsBinding.instance.addPostFrameCallback((_) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Row(
          children: <Widget>[
            const Icon(Icons.error, color: Colors.white),
            const SizedBox(width: 10),
            Text(
              content,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
        backgroundColor: type == SnackBarType.error
            ? Colors.redAccent
            : type == SnackBarType.info
                ? Colors.blueAccent
                : type == SnackBarType.success
                    ? Colors.greenAccent
                    : Colors.black,
      ),
    );
  });
}

// ignore_for_file: non_constant_identifier_names

import 'package:auto_route/auto_route.dart';

CustomRoute CustomPageRoute({
  required PageInfo page,
  bool initial = false,
  String? path,
  List<AutoRoute>? children,
}) {
  return CustomRoute(
    initial: initial,
    page: page,
    path: path,
    transitionsBuilder: TransitionsBuilders.slideLeft,
    durationInMilliseconds: 250,
    children: children,
  );
}

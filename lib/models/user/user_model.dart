import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

enum UserType { RIDER, DRIVER }

@freezed
class UserModel with _$UserModel {
  const factory UserModel({
    String? id,
    required String fullName,
    required String phone,
    required String address,
    required int age,
    required UserType type,
    required DateTime createdAt,
    required DateTime updatedAt,
  }) = _UserModel;

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);
}

class UserRepository {
  final _db = FirebaseFirestore.instance;

  Future<List> createUser(UserModel user) async {
    try {
      final res = await _db.collection('Users').add(user.toJson());
      return [res, null];
    } catch (e) {
      return [null, e];
    }
  }
}

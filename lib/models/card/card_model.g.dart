// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CardModelImpl _$$CardModelImplFromJson(Map<String, dynamic> json) =>
    _$CardModelImpl(
      id: json['id'] as String?,
      cardNumber: json['cardNumber'] as String,
      fullName: json['fullName'] as String,
      type: json['type'] as String,
      exp: json['exp'] as String,
    );

Map<String, dynamic> _$$CardModelImplToJson(_$CardModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'cardNumber': instance.cardNumber,
      'fullName': instance.fullName,
      'type': instance.type,
      'exp': instance.exp,
    };

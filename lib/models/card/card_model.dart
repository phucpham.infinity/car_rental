import 'package:freezed_annotation/freezed_annotation.dart';

part 'card_model.freezed.dart';
part 'card_model.g.dart';

@freezed
class CardModel with _$CardModel {
  const factory CardModel({
    String? id,
    required String cardNumber,
    required String fullName,
    required String type,
    required String exp,
  }) = _CardModel;

  factory CardModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$CardModelFromJson(json);
}

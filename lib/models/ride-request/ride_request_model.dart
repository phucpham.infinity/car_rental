import 'package:freezed_annotation/freezed_annotation.dart';

part 'ride_request_model.freezed.dart';
part 'ride_request_model.g.dart';

@freezed
class RideRequestModel with _$RideRequestModel {
  const factory RideRequestModel({
    String? id,
    required dynamic ride,
    required String cash,
    required String distance,
    required String phoneNumber,
    required String amount,
    required DateTime createdAt,
    required DateTime updatedAt,
    String? note,
  }) = _RideRequestModel;

  factory RideRequestModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$RideRequestModelFromJson(json);
}

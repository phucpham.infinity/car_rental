// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'ride_request_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

RideRequestModel _$RideRequestModelFromJson(Map<String, dynamic> json) {
  return _RideRequestModel.fromJson(json);
}

/// @nodoc
mixin _$RideRequestModel {
  String? get id => throw _privateConstructorUsedError;
  dynamic get ride => throw _privateConstructorUsedError;
  String get cash => throw _privateConstructorUsedError;
  String get distance => throw _privateConstructorUsedError;
  String get phoneNumber => throw _privateConstructorUsedError;
  String get amount => throw _privateConstructorUsedError;
  DateTime get createdAt => throw _privateConstructorUsedError;
  DateTime get updatedAt => throw _privateConstructorUsedError;
  String? get note => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RideRequestModelCopyWith<RideRequestModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RideRequestModelCopyWith<$Res> {
  factory $RideRequestModelCopyWith(
          RideRequestModel value, $Res Function(RideRequestModel) then) =
      _$RideRequestModelCopyWithImpl<$Res, RideRequestModel>;
  @useResult
  $Res call(
      {String? id,
      dynamic ride,
      String cash,
      String distance,
      String phoneNumber,
      String amount,
      DateTime createdAt,
      DateTime updatedAt,
      String? note});
}

/// @nodoc
class _$RideRequestModelCopyWithImpl<$Res, $Val extends RideRequestModel>
    implements $RideRequestModelCopyWith<$Res> {
  _$RideRequestModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? ride = freezed,
    Object? cash = null,
    Object? distance = null,
    Object? phoneNumber = null,
    Object? amount = null,
    Object? createdAt = null,
    Object? updatedAt = null,
    Object? note = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      ride: freezed == ride
          ? _value.ride
          : ride // ignore: cast_nullable_to_non_nullable
              as dynamic,
      cash: null == cash
          ? _value.cash
          : cash // ignore: cast_nullable_to_non_nullable
              as String,
      distance: null == distance
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      updatedAt: null == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      note: freezed == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RideRequestModelImplCopyWith<$Res>
    implements $RideRequestModelCopyWith<$Res> {
  factory _$$RideRequestModelImplCopyWith(_$RideRequestModelImpl value,
          $Res Function(_$RideRequestModelImpl) then) =
      __$$RideRequestModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? id,
      dynamic ride,
      String cash,
      String distance,
      String phoneNumber,
      String amount,
      DateTime createdAt,
      DateTime updatedAt,
      String? note});
}

/// @nodoc
class __$$RideRequestModelImplCopyWithImpl<$Res>
    extends _$RideRequestModelCopyWithImpl<$Res, _$RideRequestModelImpl>
    implements _$$RideRequestModelImplCopyWith<$Res> {
  __$$RideRequestModelImplCopyWithImpl(_$RideRequestModelImpl _value,
      $Res Function(_$RideRequestModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? ride = freezed,
    Object? cash = null,
    Object? distance = null,
    Object? phoneNumber = null,
    Object? amount = null,
    Object? createdAt = null,
    Object? updatedAt = null,
    Object? note = freezed,
  }) {
    return _then(_$RideRequestModelImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      ride: freezed == ride
          ? _value.ride
          : ride // ignore: cast_nullable_to_non_nullable
              as dynamic,
      cash: null == cash
          ? _value.cash
          : cash // ignore: cast_nullable_to_non_nullable
              as String,
      distance: null == distance
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      updatedAt: null == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      note: freezed == note
          ? _value.note
          : note // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RideRequestModelImpl implements _RideRequestModel {
  const _$RideRequestModelImpl(
      {this.id,
      required this.ride,
      required this.cash,
      required this.distance,
      required this.phoneNumber,
      required this.amount,
      required this.createdAt,
      required this.updatedAt,
      this.note});

  factory _$RideRequestModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$RideRequestModelImplFromJson(json);

  @override
  final String? id;
  @override
  final dynamic ride;
  @override
  final String cash;
  @override
  final String distance;
  @override
  final String phoneNumber;
  @override
  final String amount;
  @override
  final DateTime createdAt;
  @override
  final DateTime updatedAt;
  @override
  final String? note;

  @override
  String toString() {
    return 'RideRequestModel(id: $id, ride: $ride, cash: $cash, distance: $distance, phoneNumber: $phoneNumber, amount: $amount, createdAt: $createdAt, updatedAt: $updatedAt, note: $note)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RideRequestModelImpl &&
            (identical(other.id, id) || other.id == id) &&
            const DeepCollectionEquality().equals(other.ride, ride) &&
            (identical(other.cash, cash) || other.cash == cash) &&
            (identical(other.distance, distance) ||
                other.distance == distance) &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt) &&
            (identical(other.note, note) || other.note == note));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      const DeepCollectionEquality().hash(ride),
      cash,
      distance,
      phoneNumber,
      amount,
      createdAt,
      updatedAt,
      note);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RideRequestModelImplCopyWith<_$RideRequestModelImpl> get copyWith =>
      __$$RideRequestModelImplCopyWithImpl<_$RideRequestModelImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RideRequestModelImplToJson(
      this,
    );
  }
}

abstract class _RideRequestModel implements RideRequestModel {
  const factory _RideRequestModel(
      {final String? id,
      required final dynamic ride,
      required final String cash,
      required final String distance,
      required final String phoneNumber,
      required final String amount,
      required final DateTime createdAt,
      required final DateTime updatedAt,
      final String? note}) = _$RideRequestModelImpl;

  factory _RideRequestModel.fromJson(Map<String, dynamic> json) =
      _$RideRequestModelImpl.fromJson;

  @override
  String? get id;
  @override
  dynamic get ride;
  @override
  String get cash;
  @override
  String get distance;
  @override
  String get phoneNumber;
  @override
  String get amount;
  @override
  DateTime get createdAt;
  @override
  DateTime get updatedAt;
  @override
  String? get note;
  @override
  @JsonKey(ignore: true)
  _$$RideRequestModelImplCopyWith<_$RideRequestModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

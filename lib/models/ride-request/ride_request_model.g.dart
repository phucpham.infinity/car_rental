// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ride_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RideRequestModelImpl _$$RideRequestModelImplFromJson(
        Map<String, dynamic> json) =>
    _$RideRequestModelImpl(
      id: json['id'] as String?,
      ride: json['ride'],
      cash: json['cash'] as String,
      distance: json['distance'] as String,
      phoneNumber: json['phoneNumber'] as String,
      amount: json['amount'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      updatedAt: DateTime.parse(json['updatedAt'] as String),
      note: json['note'] as String?,
    );

Map<String, dynamic> _$$RideRequestModelImplToJson(
        _$RideRequestModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'ride': instance.ride,
      'cash': instance.cash,
      'distance': instance.distance,
      'phoneNumber': instance.phoneNumber,
      'amount': instance.amount,
      'createdAt': instance.createdAt.toIso8601String(),
      'updatedAt': instance.updatedAt.toIso8601String(),
      'note': instance.note,
    };

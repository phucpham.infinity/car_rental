import 'package:freezed_annotation/freezed_annotation.dart';

part 'vehicle_model.freezed.dart';
part 'vehicle_model.g.dart';

@freezed
class VehicleModel with _$VehicleModel {
  const factory VehicleModel({
    String? id,
    required List<String> pictures,
    required String model,
    required String year,
    required String make,
    required int seatsNumber,
    required DateTime createdAt,
    required DateTime updatedAt,
  }) = _VehicleModel;

  factory VehicleModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$VehicleModelFromJson(json);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vehicle_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$VehicleModelImpl _$$VehicleModelImplFromJson(Map<String, dynamic> json) =>
    _$VehicleModelImpl(
      id: json['id'] as String?,
      pictures:
          (json['pictures'] as List<dynamic>).map((e) => e as String).toList(),
      model: json['model'] as String,
      year: json['year'] as String,
      make: json['make'] as String,
      seatsNumber: json['seatsNumber'] as int,
      createdAt: DateTime.parse(json['createdAt'] as String),
      updatedAt: DateTime.parse(json['updatedAt'] as String),
    );

Map<String, dynamic> _$$VehicleModelImplToJson(_$VehicleModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'pictures': instance.pictures,
      'model': instance.model,
      'year': instance.year,
      'make': instance.make,
      'seatsNumber': instance.seatsNumber,
      'createdAt': instance.createdAt.toIso8601String(),
      'updatedAt': instance.updatedAt.toIso8601String(),
    };

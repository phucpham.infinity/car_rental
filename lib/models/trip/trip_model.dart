import 'package:freezed_annotation/freezed_annotation.dart';

part 'trip_model.freezed.dart';
part 'trip_model.g.dart';

@freezed
class TripModel with _$TripModel {
  const factory TripModel({
    String? id,
    required dynamic vehicle,
    required String startLocation,
    required String endLocation,
    required String time,
    required dynamic seats,
    required DateTime createdAt,
    required DateTime updatedAt,
  }) = _TripModel;

  factory TripModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$TripModelFromJson(json);
}

import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile_model.freezed.dart';
part 'profile_model.g.dart';

@freezed
class ProfileModel with _$ProfileModel {
  const factory ProfileModel({
    String? id,
    required String firstName,
    required String lastName,
    required String phoneNumber,
    required String email,
    required String password,
    required DateTime createdAt,
    required DateTime updatedAt,
    String? address,
  }) = _ProfileModel;

  factory ProfileModel.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$ProfileModelFromJson(json);
}

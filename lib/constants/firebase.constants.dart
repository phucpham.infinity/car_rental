class FirebaseConstants {
  static const usersCollection = "users";
  static const cardsCollection = "cards";
  static const profileCollection = "profile";
}
